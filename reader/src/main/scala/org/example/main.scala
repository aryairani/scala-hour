package org.example

object GlobalConfig {
  type Config = Map[String,String]

  val map = Map("firstName" -> "Kate","lastName" -> "Winslet","pickupLine" -> "What’s wrong? You’re looking a little sad and gloomy. What you need is some, Vitamin Me.")
}

object ReaderEx1 {

  def getFirstName : String = GlobalConfig.map("firstName")
  def getLastName : String = GlobalConfig.map("lastName")
  def getPickupLine : String = GlobalConfig.map("pickupLine")
  def sayHello(firstName : String, lastName : String) : String = "Hello, " + firstName + " " + lastName + "!\n" + getPickupLine

  def run() {
    val s = sayHello(getFirstName, getLastName)
    println(s)
  }
  // Pro: global config doesn't clutter up function parameters
  // Con: can't tell when a function uses global config - no referential transparency
  // Problem: globalConfig should be immutable, but that means it can never be changed at runtime!
}

object ReaderEx2 {
  import GlobalConfig.Config

  def getFirstName(config : Config) : String = config("firstName")
  def getLastName(config : Config) : String = config("lastName")
  def getPickupLine(config : Config) : String = config("pickupLine")
  def sayHello(firstName : String, lastName : String, pickupLine : String) : String = "Hello, " + firstName + " " + lastName + "!\n" + pickupLine

  def run() {
    val s = sayHello(getFirstName(GlobalConfig.map), getLastName(GlobalConfig.map), getPickupLine(GlobalConfig.map))
    println(s)

    val localConfig = Map("firstName" -> "Amanda","lastName" -> "Seyfried","pickupLine" -> "Your body's name must be Visa, because it's everywhere I want to be.")
    val s2 = sayHello(getFirstName(localConfig), getLastName(localConfig),getPickupLine(localConfig))
    println(s2)
  }
  // Pro: simple and obvious pattern - no deep mysteries here
  // Pro: Can change config at run time - global, read from file, database, etc
  // Pro: can tell exactly the parameters a function depends on - good referential transparency
  // Con: must pass config variable to functions that use it
  // Con: must pass config variable even to functions that don't use it directly
  // Problems: none
}

object ReaderEx3 {
  import GlobalConfig.Config

  abstract class Reader[+A] {
    // This is intentionally undefined
    def apply(config : Config) : A

    def flatMap[B](f: A => Reader[B]) : Reader[B] =
      // return an anonymous class
      new Reader[B] {
        // define apply
        def apply(config : Config) = {
          // get A from outer apply with config
          val outer : A = Reader.this.apply(config)
          // use f to convert to a Reader[B]
          val inner : Reader[B] = f(outer)
          // extract B from apply with config
          inner.apply(config)
        }
      }

    def map[B](f: A => B) : Reader[B] =
      // return an anonymous class
      new Reader[B] {
        // define apply
        def apply(config : Config) = {
          // get A from outer apply with config
          val outer : A = Reader.this.apply(config)
          // use f to convert A to a B
          f(outer)
        }
      }
  }
  object Reader {
    // Special function used to "ask" for a configuration
    def ask =
      // return an anonymous class
      new Reader[Config] {
        // since this is a Reader of Config just return config!
        def apply(config: Config) : Config = config
      }
  }

  import Reader.ask

  def getFirstName : Reader[String] = {
    // desugared for readability
    ask.map({
      config =>
        config("firstName")
    })
  }
  def getLastName : Reader[String] = {
    // sugared version
    for {
      config <- ask
    } yield config("lastName")
  }
  def getPickupLine : Reader[String] = for(config <- ask) yield config("pickupLine")

    // Problem: sayHello shouldn't have to be modified to take Reader!
  def sayHello(firstName : String, lastName : String, pickupLine : String) : String = "Hello, " + firstName + " " + lastName + "!\n" + pickupLine

  // Solution: higher-order function lift to the rescue!
  def lift3[A,B,C,Z](f: (A,B,C) => Z) : (Reader[A],Reader[B],Reader[C]) => Reader[Z] =
    // Desugared for readability
    new Function3[Reader[A],Reader[B],Reader[C],Reader[Z]] {
      def apply(aa: Reader[A], bb: Reader[B], cc: Reader[C]) = {
        for {
          a <- aa
          b <- bb
          c <- cc
        } yield f(a,b,c)
      }
    }

  def run() {
    // Lift sayHello to accept Reader parameters
    val liftedSayHello = lift3(sayHello)
    // Call lifted sayHello
    val reader : Reader[String] = liftedSayHello(getFirstName, getLastName, getPickupLine)
    // Apply some configuration to the Reader result to extract the string
    val s : String = reader.apply(GlobalConfig.map)
    println(s)

    val localConfig = Map("firstName" -> "Tina","lastName" -> "Fey","pickupLine" -> "Did you know they changed the alphabet? They put U and I together.")
    // Note: didn't have to call liftedSayHello again! Just reapply new configuration
    val s2 = reader(localConfig)
    println(s2)
  }

  // Pro: Can change config at run time - global, read from file, database, etc
  // Pro: Doesn't clutter method parameters with a config variable
  // Pro: Return type explicitly shows when configuration is needed - good referential transparency
  // Pro: lift allows working with methods that don't take Reader
  // Con: Must wrap return type with Reader
  // Con: Must have good library support for Reader/lift
  // Con: Uses a monad pattern that must be well understood by maintainers
}

// Dependency Injection example
object ReaderEx4 {
  // A generic Reader
  abstract class Reader[E, +A] { self =>
    def apply(e : E) : A
    def flatMap[B](f: A => Reader[E,B]) : Reader[E,B] =
      new Reader[E,B] {
        def apply(e : E) = f(self(e))(e)
      }

    def map[B](f: A => B) : Reader[E,B] =
      new Reader[E,B] {
        def apply(e : E) = f(self(e))
      }
  }
  object Reader {
    def ask[E] =
      new Reader[E,E] {
        def apply(e: E) : E = e
      }
    def apply[E,A](a : A) =
    new Reader[E,A] {
      def apply(e : E) : A = a
    }
  }
  import Reader.ask

  trait FlyBehaviour {
    def fly()
  }
  trait QuackBehaviour {
    def quack()
  }
  trait Animal {
    def move()
    def makeSound()
  }

  class Duck(val flyBehaviour: FlyBehaviour, val quackBehaviour: QuackBehaviour) extends Animal {
    def quack() = quackBehaviour.quack()
    def fly() = flyBehaviour.fly()
    def move() = fly()
    def makeSound() = quack()
  }

  case class Zoo(val animal: Animal)

  trait AnimalConfig {
    def fly: FlyBehaviour
    def quack: QuackBehaviour
  }

  object DefaultConfig extends AnimalConfig {
    def fly = new FlyBehaviour {
      def fly() {
        println("fly!")
      }
    }

    def quack = new QuackBehaviour {
      def quack() {
        println("quack!")
      }
    }
  }
  type Env[+A] = Reader[AnimalConfig, A]

  def run() {
    val z : Env[Zoo] = for {
      c <- ask
      a <- Reader(new Duck(c.fly, c.quack))
    } yield new Zoo(a)

    val zoo : Zoo = z(DefaultConfig)
    zoo.animal.makeSound()
    zoo.animal.move()
  }
}

object ReaderEx5 {
  // A Reader is really just a function that takes the config and returns a value.
  type Reader[-E,+A] = (E => A)

  object Reader {
    def apply[E,A](a: A): Reader[E,A] = (_: E) => a
  }

  implicit class ReaderSyntax[E,A](self: Reader[E,A]) {
    def map[B](f: A => B)               : Reader[E,B] =  e => f(self(e))
    def flatMap[B](f: A => Reader[E,B]) : Reader[E,B] =  e => f(self(e)).apply(e)
  }

  object ReaderSyntax {
    def ask[E]: Reader[E,E] = e => e
    def local[E,A](r: Reader[E,A])(f: E => E): Reader[E,A] = (r compose f)
  }

  import GlobalConfig.Config
  import ReaderSyntax._

  // desugared for readability
  def getFirstName: Reader[Config,String] = ask[Config].map(config => config("firstName"))
  // sugared version
  def getLastName: Config => String = for (c <- ask) yield c("lastName")
  def getPickupLine: Config => String = for (c <- ask) yield c("pickupLine")

  // Problem: sayHello shouldn't have to be modified to take Reader!
  def sayHello(firstName : String, lastName : String, pickupLine : String) : String = s"Hello, $firstName $lastName! $pickupLine"

  def lift3[E,A,B,C,Z](f: (A,B,C) => Z) : (E=>A, E=>B, E=>C) => (E=>Z) =
    (aa, bb, cc) => for (a <- aa; b <- bb; c <- cc) yield f(a,b,c)

  def run() {
    type C[+A] = Reader[Config,A] // Config => A

    // Lift sayHello to accept Reader parameters
    val liftedSayHello: (C[String],C[String],C[String]) => C[String] = lift3(sayHello)

    // Call lifted sayHello
    val helloWithConfig: C[String] = liftedSayHello(getFirstName, getLastName, getPickupLine)

    // Run the reader with a config
    println(helloWithConfig(GlobalConfig.map))

    val localConfig: Config = Map("firstName" -> "Tina","lastName" -> "Fey","pickupLine" -> "Did you know they changed the alphabet? They put U and I together.")
    // Note: didn't have to call liftedSayHello again! Just reapply new configuration
    println(helloWithConfig(localConfig))

    // `local` lets you locally modify the environment for an underlying reader. (maybe a corner case)
    def describeBotchedHello = for {
      intendedLine <- helloWithConfig
      botchedLine <- local(helloWithConfig) {
        c => c.updated("pickupLine", s"Did you know... uhmm.. ${c("pickupLine")}")
      }
    } yield s"I meant to say: '$intendedLine', but I ended up saying: '$botchedLine'."

    println(describeBotchedHello(GlobalConfig.map))
  }

  // This version just emphasizes that a Reader[E,A] is really just a function E => A, and represents a computation
  //      that's ready to go, provided an environment/configuration is available/provided.
  // Pro: Can change config at run time - global, read from file, database, etc
  // Pro: Doesn't clutter method parameters with a config variable
  // Pro: Return type explicitly shows when configuration is needed - good referential transparency
  // Pro: lift allows working with methods that don't take Reader
  // Con: Must wrap return type with Reader or Function
  // Con: Must have good library support for Reader/lift
  // Con: Uses a monad pattern that must be well understood by maintainers
}