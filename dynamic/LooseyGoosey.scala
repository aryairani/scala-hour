import java.util.Date
import scala.language.dynamics

class LooseyGoosey extends Dynamic {
  def applyDynamic(method: String) = method match {
    case "display" => (s:String) =>
      println(s)
    case "displayBig" => (s:String) =>
      println(s.toUpperCase)
//    case "displayExcited" => (s: String, degree: Int) =>
//      println(s + "!" * degree)
  }

  def selectDynamic(field: String) = field match {
    case "clock" => System.currentTimeMillis()
  }

  def applyDynamicNamed(method: String)(args: (String,Any)*) = method match {
    case "checksum" => {
      val a = getNamedArgs(List("string", "seed"), args)
      val (string, seed) = (a("string"),a("seed")).asInstanceOf[(String,Int)]
//      val string = a("string").asInstanceOf[String]
//      val seed = a("seed").asInstanceOf[Int]
      string.foldLeft(seed) { _ + _ }
    }
  }

  def getNamedArgs(declaredOrder: Seq[String],
                      params: Seq[(String,Any)],
                      accum: Map[String,Any] = Map()): Map[String,Any] = {

    require(declaredOrder.length == params.length, "Incorrect number of arguments.")
    params.toList match {
      case Nil => accum
      case ("",v) :: paramsTail => // unnamed parameter, use the next expected name from the list
        getNamedArgs(declaredOrder.tail, paramsTail, accum + (declaredOrder.head -> v))
      case (k,v) :: paramsTail => // named parameter, make sure not to expect it anymore
        require(declaredOrder.contains(k), s"Unknown named argument '$k'")
        getNamedArgs(declaredOrder.filterNot(_ == k), paramsTail, accum + (k -> v))
    }
  }
}

object Main extends App {
  val lg = new LooseyGoosey
  lg.display("You smell.") // prints to stdout
//  lg.displayExcited("Just kidding", 5) // prints an important message
  println {
    lg.checksum("I bet IDE support will take a while to catch up.", seed = 5)
  }
  println(s"the time is now: ${lg.clock}") // get current time in millis
}