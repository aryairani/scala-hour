package j;

public class HasUpperCaseDemo {

    public static void main(String[] args) {

        String name = "HasUpperCaseDemo";

        boolean hasUpperCase = false;
        for (int i = 0; i < name.length(); i++) {
            if (Character.isUpperCase(name.charAt(i))) {
                hasUpperCase = true;
                break;
            }
        }

        System.out.println(hasUpperCase);
    }
}
