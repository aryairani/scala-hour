package s

object BreakWithLabelDemo2 extends App {

  val matrix = Array(
    Array( 32, 87, 3, 589 ),
    Array( 12, 1076, 2000, 8 ),
    Array( 622, 127, 77, 955 )
  )

  val searchFor = 12

  def find(value: Int) = {
    val values = for {
      (row, i) <- matrix.view.zipWithIndex
      (v, j) <- row.view.zipWithIndex
    } yield (v, i, j)

    values.collectFirst { case (v, i, j) if (v == value) => (i, j) }
  }

  find(searchFor) match {
    case Some((i,j)) => println(s"Found $searchFor at $i, $j")
    case None => println(s"$searchFor not in the array")
  }

}
