package s

object ContinueWithLabelDemo extends App {
   val searchMe = "Look for a substring in me"
   val substring = "sub"
   val foundIt = searchMe.contains(substring)
   println(if (foundIt) "Found it" else "Didn't find it")
 }
