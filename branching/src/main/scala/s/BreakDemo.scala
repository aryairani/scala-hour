package s

object BreakDemo extends App {

  val arrayOfInts = Array(32, 87, 3, 589, 12, 1076, 2000, 8, 622, 127 )
  val searchFor = 12
  val i = arrayOfInts.indexWhere( _ == searchFor )

  if (i >= 0)
    println(s"Found $searchFor at index $i")
  else
    println(s"$searchFor not in the array")
}
