package s

object HasUpperCaseDemo extends App {
  val name = "HasUpperCaseDemo"
  val hasUpperCase = name.exists( _.isUpper )
  println(hasUpperCase)
}