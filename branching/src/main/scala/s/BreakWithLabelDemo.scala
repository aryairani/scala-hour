package s

object BreakWithLabelDemo extends App {

  val arrayOfInts = Array( Array( 32, 87, 3, 589 ),
    Array( 12, 1076, 2000, 8 ),
    Array( 622, 127, 77, 955 )
  )

  val searchFor = 12

  val i = arrayOfInts.indexWhere( _ contains searchFor )
  val j = if (i < 0) -1 else arrayOfInts(i).indexWhere( _ == searchFor )
  val foundIt = i >=0 && j >= 0

  if (i >= 0 && j >= 0)
    println(s"Found $searchFor at $i, $j")
  else
    println(s"$searchFor not in the array")
}
