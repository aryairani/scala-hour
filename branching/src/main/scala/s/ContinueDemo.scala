package s

object ContinueDemo extends App {
   val searchMe = "peter piper picked a peck of pickled peppers"
   val numPs = searchMe.count( _ == 'p' )
   println(s"Found $numPs p's in the string.")
 }
