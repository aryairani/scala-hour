class FactInt(val x:Int) {
  def !():FactInt = {
    if(x==0) 1
    else x * ((x-1)!)
  }

  implicit def intWrapper(i:Int):FactInt = new FactInt(i) 
  implicit def factIntWrapper(i:FactInt):Int = i.x
}

implicit def intWrapper(i:Int):FactInt = new FactInt(i) 
implicit def factIntWrapper(i:FactInt):Int = i.x

def factorial(x:Int):Int = {
  if(x==0) 1
  else x * factorial(x-1)
}

val n = new FactInt(6)
factorial(n)
var t = (n!).x
var i:Int = n!

val m = 6
factorial(m)
var t1 = (m!).x
var i1:Int = m!
