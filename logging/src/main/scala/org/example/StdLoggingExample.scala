package org.example

object GlobalLogger {
  var log : Logger = new NullLogger
}

// Logging as it is commonly implemented using a global variable that can modified at run time
object StdLoggingExample {
  import GlobalLogger._

  def foo(i : Int, j : Int) : Int = {
    log trace s"foo(i=$i,j=$j)"
    log debug "debug message"
    val retv = i + j
    log trace s"foo => $retv"
    retv
  }
  
  def bar(s : String) : String = {
    log trace s"bar(s=$s)"
    log info "about to foo"
    val result = (1 to 10).foldLeft("") { (acc, i) => acc + foo(i, i) }
    log info s"foo result=$result"
    val retv = result + s
    log trace s"foo => $retv"
    retv
  }

  def run {
    val result = bar("suffix")
    println(s"some output=$result")
  }

  // PRO: simple
  // PRO: many supporting libraries
  // CON: no referential transparency
  // CON: side effects
  // CON: uses a global variable
}
