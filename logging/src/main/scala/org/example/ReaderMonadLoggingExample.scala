package org.example

import scalaz._
import Scalaz._

// Logging using scalaz Reader monad
object ReaderMonadLoggingExample {
  type LogReader[+A] = Reader[Logger, A]
  object LogReader {
    def apply[A](a : A) = {
      val f : Logger => A = { (l : Logger) => a }
      Reader(f)
    }
  }
  object LogReaderHelpers {
    def ask : LogReader[Logger] = Reader(identity)
  }
  import LogReaderHelpers._

  def foo(i : Int, j : Int) : LogReader[Int] = {
    for(log <- ask) yield {
      log trace s"foo(i=$i,j=$j)"
      log debug "debug message"
      val retv = i + j
      log trace s"foo => $retv"
      retv
    }
  }

  def bar(s : String) : LogReader[String] = {
    for(log <- ask) yield {
      log trace s"bar(s=$s)"
      log info "about to foo"
      val result = (1 to 10).foldLeft("") { (acc, i) =>
        acc + foo(i,i)(log)
      }
      log info s"foo result=$result"
      val retv = result + s
      log trace s"foo => $retv"
      retv
    }
  }

  def run : LogReader[Unit] = {
    for(result <- bar("suffix"))
      yield println(s"some output=$result")
  }

  // PRO: referential transparency
  // PRO/CON: side effects during computation but are controlled by caller
  // CON: performance hit - each function call is actually a function generator and results in allocating a function object
}
