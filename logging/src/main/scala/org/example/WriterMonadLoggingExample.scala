package org.example

import scalaz._
import Scalaz._

// Logging using scalaz Writer monad
object WriterMonadLoggingExample {
  
  type LogWriter[+A] = Writer[List[LogEntry], A]
  object LogWriter {
    def apply[A](a : A) = Writer(List[LogEntry](), a)
    def apply[A](log : List[LogEntry], a : A) = Writer(log, a)
    def apply(logEntry : LogEntry) : LogWriter[Unit] = Writer(logEntry :: Nil, ())
  }
  object LogHelpers {
    def trace(message: => String) : LogWriter[Unit] = LogWriter(LogEntry(LogLevel.TRACE, message))
    def debug(message: => String) = LogWriter(LogEntry(LogLevel.DEBUG, message))
    def info(message: => String) = LogWriter(LogEntry(LogLevel.INFO, message))
    def warn(message: => String) = LogWriter(LogEntry(LogLevel.WARN, message))
    def error(message: => String) = LogWriter(LogEntry(LogLevel.ERROR, message))
    def severe(message: => String) = LogWriter(LogEntry(LogLevel.SEVERE, message))
  }
  import LogHelpers._

  def foo(i : Int, j : Int) : LogWriter[Int] = {
    for {
      _ <- trace(s"foo(i=$i,j=$j)")
      _ <- debug("debug message")
      retv <- LogWriter( i + j )
      _ <- trace(s"foo => $retv")
    } yield retv
  }

  def bar(s : String) : LogWriter[String] = {
    for {
      _ <- trace(s"bar(s=$s)")
      _ <- info("about to foo")
      result <- (1 to 10).foldLeft(LogWriter("")) { (writerAcc, i) =>
        for {
          acc <- writerAcc
          v <- foo(i,i)
        }
        yield acc + v
      }
      _ <- info(s"foo result=$result")
      retv <- LogWriter(result + s)
      _ <- trace(s"foo => $retv")
    } yield retv
  }

  def run : LogWriter[Unit] = {
    for {
      result <- bar("suffix")
    } yield {
      println(s"some output=$result")
    }
  }

  // PRO: referential transparency
  // PRO: no side effects
  // CON: scala has weak syntatic sugar support for monadic idioms (everything must use for-comprehension)
  // CON: performance hit - every line in for-comprehension creates a new LogWriter and calls flatMap/map and creates/appends log entries
  // CON: maintainers must have clear understanding of Writer monad and standard idioms
  // CON: infinite loop bug results in no output
  // CON: log entries take up space in memory until the log is flushed
  // CON: when to flush the log? program exit?
}

