package org.example

// Logging as it is commonly implemented but using a local implicit variable instead of a global
object ImplicitLoggingExample {

  def foo(i : Int, j : Int)(implicit log : Logger) : Int = {
    log trace s"foo(i=$i,j=$j)"
    log debug "debug message"
    val retv = i + j
    log trace s"foo => $retv"
    retv
  }

  def bar(s : String)(implicit log : Logger) : String = {
    log trace s"bar(s=$s)"
    log info "about to foo"
    val result = (1 to 10).foldLeft("") { (acc, i) => acc + foo(i, i) }
    log info s"foo result=$result"
    val retv = result + s
    log trace s"foo => $retv"
    retv
  }

  def run(implicit log : Logger) {
    val result = bar("suffix")
    println(s"some output=$result")
  }

  // PRO: simple
  // PRO: referential transparency
  // PRO/CON: side effects during computation but are controlled by caller
}
