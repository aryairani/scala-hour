package org.example

trait Logger {
  def trace(message: => String)
  def debug(message: => String)
  def info(message: => String)
  def warn(message: => String)
  def error(message: => String)
  def severe(message: => String)
}

class NullLogger extends Logger {
  // To add some performance delay
  class NullOutputStream extends java.io.OutputStream {
    override def write(b : Int) { }
  }
  val writer = new java.io.PrintWriter(new NullOutputStream)
  def doLog(level: String, message: String) { writer.print(s"[$level] $message\n") }

  def trace(message: => String) { doLog("TRACE",message) }

  def debug(message: => String) { doLog("DEBUG",message) }

  def info(message: => String) { doLog("INFO",message) }

  def warn(message: => String) { doLog("WARN",message) }

  def error(message: => String) { doLog("ERROR",message) }

  def severe(message: => String) { doLog("SEVERE",message) }

}

class ListLogger extends Logger {
  private var list : List[LogEntry] = Nil
  def get : List[LogEntry] = list
  def append(rhs : List[LogEntry]) { list :::= rhs }
  def :~>[A](a : A) : (List[LogEntry], A) = (list, a)
  def trace(message: => String) { list ::= LogEntry(LogLevel.TRACE, message) }
  def debug(message: => String) { list ::= LogEntry(LogLevel.DEBUG, message) }
  def info(message: => String) { list ::= LogEntry(LogLevel.INFO, message) }
  def warn(message: => String) { list ::= LogEntry(LogLevel.WARN, message) }
  def error(message: => String) { list ::= LogEntry(LogLevel.ERROR, message) }
  def severe(message: => String) { list ::= LogEntry(LogLevel.SEVERE, message) }
}

class ConsoleLogger extends Logger {

  def trace(message: => String) { doLog("TRACE",message) }

  def debug(message: => String) { doLog("DEBUG",message) }

  def info(message: => String) { doLog("INFO",message) }

  def warn(message: => String) { doLog("WARN",message) }

  def error(message: => String) { doLog("ERROR",message) }

  def severe(message: => String) { doLog("SEVERE",message) }

  def doLog(level: String, message: String) { println(s"[$level] $message") }
}
