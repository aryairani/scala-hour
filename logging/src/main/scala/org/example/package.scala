package org

package object example {
  val TIME_TRIAL_N = 100000

  def runAllTimeTrials {
    println("StdLoggingExample")
    runTimeTrial {
      StdLoggingExample.bar("suffix")
    }
    println("ImplicitLoggingExample")
    implicit val logger = new NullLogger
    runTimeTrial {
      ImplicitLoggingExample.bar("suffix")
    }
    println("ListLoggingExample")
    runTimeTrial {
      val (log, result) = ListLoggingExample.bar("suffix")
      for(entry <- log) {
        entry.pushTo(logger)
      }
    }
    println("WriterMonadLoggingExample")
    runTimeTrial {
      val (log,result) = WriterMonadLoggingExample.bar("suffix").run
      for(entry <- log) {
        entry.pushTo(logger)
      }
    }
    println("ReaderMonadLoggingExample")
    runTimeTrial {
      ReaderMonadLoggingExample.bar("suffix")(logger)
    }
  }

  def runTimeTrial[A](f: => Unit) {
    val startTime = System.nanoTime()
    for(i <- 1 to TIME_TRIAL_N) {
      f
    }
    val endTime = System.nanoTime()
    println("Time (ms)=" + (endTime - startTime) / 1000000d)
  }

}
