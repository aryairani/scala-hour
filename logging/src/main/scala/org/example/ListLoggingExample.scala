package org.example

// Logging using a scala List and some syntatic sugar to make it look like normal logging
object ListLoggingExample {

  def foo(i : Int, j : Int) : (List[LogEntry], Int) = {
    val log = new ListLogger
    log trace s"foo(i=$i,j=$j)"
    log debug "debug message"
    val retv = i + j
    log trace s"foo => $retv"
    log :~> retv
  }

  def bar(s : String) : (List[LogEntry], String) = {
    val log = new ListLogger
    log trace s"bar(s=$s)"
    log info "about to foo"
    val (resultLog, result) = (1 to 10).foldLeft((List[LogEntry](),"")) { (acc, i) =>
      val (accLog, accValue) = acc
      val (log, result) = foo(i,i)
      (log ::: accLog, result + accValue)
    }
    log append resultLog
    log info s"foo result=$result"
    val retv = result + s
    log trace s"foo => $retv"
    log :~> retv
  }

  def run : List[LogEntry] = {
    val (resultLog, result) = bar("suffix")
    println(s"some output=$result")
    resultLog
  }

  // PRO: referential transparency
  // PRO: no side effects
  // CON: awkward accumulation of logs from nested functions
  // CON: awkward extraction of result (and log)
  // CON: performance hit - log entries are constantly created and appended as functions are called
  // CON: infinite loop bug results in no output
  // CON: log entries take up space in memory until the log is flushed
  // CON: when to flush the log? program exit?
}
