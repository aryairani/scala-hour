package org.example

object LogLevel extends Enumeration {
  type LogLevel = Value
  val TRACE, DEBUG, INFO, WARN, ERROR, SEVERE = Value
}

class LogEntry(val level : LogLevel.LogLevel, _message: => String) {
  def message = _message
  override def toString = s"LogEntry(level=$level,message=$message)"
  def pushTo(logger : Logger) {
    level match {
      case LogLevel.TRACE => logger.trace(message)
      case LogLevel.DEBUG => logger.debug(message)
      case LogLevel.INFO => logger.info(message)
      case LogLevel.WARN => logger.warn(message)
      case LogLevel.ERROR => logger.error(message)
      case LogLevel.SEVERE => logger.severe(message)
    }
  }
}
object LogEntry {
  def apply(level : LogLevel.LogLevel, message: => String) = new LogEntry(level, message)
}

