libraryDependencies +=   "org.scalaz" %% "scalaz-core" % "7.1.0-M6"

libraryDependencies +=   "org.scalaz" %% "scalaz-effect" % "7.1.0-M6"

scalaVersion := "2.10.4"

scalacOptions += "-feature"