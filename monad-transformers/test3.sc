import scalaz._
import scalaz.Id
import scalaz.Scalaz._

type SST[F[_],A] = StateT[F,String,A]
type IST[Z[_],A] = StateT[Z,Int,A]
val p1: StateT[Id,Int,Unit] = modify[Int](1+)
val p2: StateT[Id,String,Unit] = modify[String](_ + "1")
val p3: StateT[({type l[a]=StateT[Id,String,a]})#l,Int,Unit] = p2.liftM[IST]

/*

> test3 = do
>     modify (+ 1)
>     lift $ modify (++ "1")
>     a <- get
>     b <- lift get
>     return (a,b)

> go3 = runIdentity $ evalStateT (evalStateT test3 0) "0"

 */

/** solution from StackExchange */
def test3se1[M[_]:Monad](implicit inner: MonadState[({ type T[s,a]=StateT[M,s,a] })#T, String],
                         outer: MonadState[({type T[s,a]=StateT[({ type L[y]=StateT[M,String,y]})#L,s,a]})#T,Int],
                         mt: MonadTrans[({type L[f[_],a]=StateT[f,Int,a]})#L]) =
  for {
    _ <- outer.modify(_+1)
    _ <- mt.liftMU(inner.modify(_+"1"))
    a <- outer.get
    b <- mt.liftMU(inner.get)
  } yield (a,b)
//val go3se1 = test3se1[Id].eval(0).eval("0")

def test3se2[M[_]: Monad](implicit
                          inner: MonadState[({ type T[s, a] = StateT[M, s, a] })#T, String],
                          mt: MonadTrans[({ type L[f[_], a] = StateT[f, Int, a] })#L]
                           ) = {
  val outer =
    StateT.stateTMonadState[Int, ({ type L[y] = StateT[M, String, y] })#L]

  for {
    _ <- outer.modify(_ + 1)
    _ <- mt.liftMU(inner.modify(_ + "1"))
    a <- outer.get
    b <- mt.liftMU(inner.get)
  } yield (a, b)
}
val go3se2 = test3se2[Id].eval(0).eval("0")


//val sm = StateT.stateTMonadState
import scalaz.Lens.{firstLens,secondLens}
val test3lens = for {
  _ <- firstLens[Int,String] lifts (modify (1+))
  _ <- secondLens[Int,String] lifts (modify (_ + "1"))
  a <- firstLens[Int,String] lifts get
  b <- secondLens[Int,String] lifts get
} yield (a,b)

val go3lens = test3lens.eval(0,"0")
