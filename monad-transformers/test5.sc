import scala.language.{postfixOps,higherKinds}
import scalaz._
import scalaz.Scalaz._
import scalaz.effect.IO

/*
test5 :: StateT Integer IO ()
> test5 = do
>     modify (+ 1)
>     a <- get
>     lift (print a)
>     modify (+ 1)
>     b <- get
>     lift (print b)

> go5 = evalStateT test5 0
*/

val ms = StateT.stateTMonadState[Int,IO]
/** MonadTrans lets you go from G[A] to F[G,A] (some Transformer),
  * (if you can figure out what F is)
  * i.e. IO[A] to StateT[IO,_,A] */
val mt = MonadTrans[({ type L[f[_],a] = StateT[f,Int,a]})#L]
val test5: StateT[IO, Int, Unit] = for {
  _ <- ms.modify(_ + 1)
  a <- ms.get
  _ <- mt.liftMU(IO.putLn(a))
  _ <- ms.modify(_ + 1)
  b <- ms.get
  _ <- mt.liftMU(IO.putLn(b))
} yield ()
val go5 = test5.eval(0)
go5.unsafePerformIO()



