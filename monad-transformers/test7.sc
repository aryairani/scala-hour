import scalaz._, scalaz.Scalaz._
import scala.language.{postfixOps,higherKinds}


/*

test7 :: StateT Integer (StateT [Char] Identity) (Integer, [Char])

> test7 = do
>     modify (+ 1)
>     lift $ modify (++ "1")
>     a <- get
>     b <- lift get
>     return (a,b)

> go7 = evalState (evalStateT test7 0) "0"

 */

type SM[S] = { type L[a]=State[S,a] }
val sm = StateT.stateMonad[String]
val ms = StateT.stateTMonadState[Int,SM[String]#L]
/** MonadTrans lets you go from G[A] to F[G,A] (some Transformer)
  *                           if you can figure out what F is.
  * i.e. State[String,_] to StateT[State[String,_],Int,_] */
val mt = MonadTrans[({ type L[f[_],a] = StateT[f,Int,a]})#L]
val test5: StateT[SM[String]#L, Int, (Int,String)] = for {
  _ <- ms.modify(_ + 1)
  _ <- mt.liftM[SM[String]#L,](sm.modify(_ + "1"))
  a <- ms.get
  b <- mt.liftMU(sm.get)
} yield (a,b)

val go7 = test5.eval(0)
