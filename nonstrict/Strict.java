public class Strict {

    // can't pass values of type `void` in Java.
    static int onlyIfTrue() {
        System.out.println("condition was true");
        return 0;
    }

    static int onlyIfFalse() {
        System.out.println("condition was false");
        return 0;
    }

    static <T> T brokenIf(boolean condition, T trueValue, T falseValue) {
        if (condition)
            return trueValue;
        else
            return falseValue;
    }

    // static <T> T workingIf(boolean condition, ... )

    public static void main(String[] args) {
        final boolean b = false;

        System.out.println("standard if:");
        if (b)
            onlyIfTrue();
        else
            onlyIfFalse();

        System.out.println();
        System.out.println("brokenIf:");
        brokenIf(b, onlyIfTrue(), onlyIfFalse());

        /*
        System.out.println()
        System.out.println("workingIf:");
        workingIf(b, onlyIfTrue(), onlyIfFalse());
         */
    }
}