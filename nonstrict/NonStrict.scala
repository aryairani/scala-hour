object NonStrict {

  def onlyIfTrue() {
    println("condition was true")
  }

  def onlyIfFalse() {
    println("condition was false")
  }


  // call by value for all parameters
  def brokenIf[A](condition: Boolean, trueValue: A, falseValue: A) =
    if (condition)
      trueValue
    else
      falseValue

  // call by value for `condition`; call by name for `trueValue` and `falseValue`
  def workingIf[A](condition: Boolean, trueValue: => A, falseValue: => A) =
    if (condition)
      trueValue
    else
      falseValue


  def main(args: Array[String]) {
    val b = false

    println("standard if:")
    if (b) onlyIfTrue() else onlyIfFalse()

    println()
    println("brokenIf:")
    brokenIf(b, onlyIfTrue(), onlyIfFalse())

    println()
    println("workingIf:")
    workingIf(b, onlyIfTrue(), onlyIfFalse())
  }

}