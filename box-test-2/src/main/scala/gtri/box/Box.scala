package gtri.box

import scalaz.{OptionT,Writer}

object Box {
  def apply[A](m: ListWriter[Option[A]]): Box[A] =
    OptionT(m) // IDEA erroneously shows an error

  def apply[A](a : Option[A], issues : List[Issue]): Box[A] = {
    val writer: ListWriter[Option[A]] = Writer(issues,a)
    Box(writer)
  }

  def empty[A]: Box[A]                              = Box(Option.empty[A], Nil)
  def empty[A](issue: Issue): Box[A]                = Box(None, List(issue))
  def empty[A](issues: List[Issue]): Box[A]         = Box(None, issues)
  def apply[A](a: A): Box[A]                        = Box(Some(a), Nil)
  def apply[A](a: A, issue: Issue): Box[A]          = Box(Some(a), List(issue))
  def apply[A](a: A, issues: List[Issue]): Box[A]   = Box(Some(a), issues)
}


