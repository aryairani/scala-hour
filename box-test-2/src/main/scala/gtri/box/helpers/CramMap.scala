package gtri.box.helpers

import gtri.box._
import scalaz.{Apply, OptionT}
import scalaz.std.list.listMonoid
import scalaz.std.option.optionInstance

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/15/12
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
object CramMap {
  def cramMap[A,Z](ba: Box[A])(f: (A) => Z): Box[Z] = OptionT(Apply[ListWriter].apply(ba.run)(Apply[Option].lift(f)))
  def cramMap[A,B,Z](ba: Box[A],bb: Box[B])(f: (A,B) => Z): Box[Z] = OptionT(Apply[ListWriter].apply2(ba.run, bb.run)(Apply[Option].lift2(f)))
  def cramMap[A,B,C,Z](ba: Box[A],bb: Box[B],bc: Box[C])(f: (A,B,C) => Z): Box[Z] = OptionT(Apply[ListWriter].apply3(ba.run, bb.run, bc.run)(Apply[Option].lift3(f)))
  def cramMap[A,B,C,D,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D])(f: (A,B,C,D) => Z): Box[Z] = OptionT(Apply[ListWriter].apply4(ba.run, bb.run, bc.run, bd.run)(Apply[Option].lift4(f)))
  def cramMap[A,B,C,D,E,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E])(f: (A,B,C,D,E) => Z): Box[Z] = OptionT(Apply[ListWriter].apply5(ba.run, bb.run, bc.run, bd.run, be.run)(Apply[Option].lift5(f)))
  def cramMap[A,B,C,D,E,F,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F])(f: (A,B,C,D,E,F) => Z): Box[Z] = OptionT(Apply[ListWriter].apply6(ba.run, bb.run, bc.run, bd.run, be.run, bf.run)(Apply[Option].lift6(f)))
  def cramMap[A,B,C,D,E,F,G,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G])(f: (A,B,C,D,E,F,G) => Z): Box[Z] = OptionT(Apply[ListWriter].apply7(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run)(Apply[Option].lift7(f)))
  def cramMap[A,B,C,D,E,F,G,H,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G],bh: Box[H])(f: (A,B,C,D,E,F,G,H) => Z): Box[Z] = OptionT(Apply[ListWriter].apply8(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run, bh.run)(Apply[Option].lift8(f)))
  def cramMap[A,B,C,D,E,F,G,H,I,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G],bh: Box[H],bi: Box[I])(f: (A,B,C,D,E,F,G,H,I) => Z): Box[Z] = OptionT(Apply[ListWriter].apply9(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run, bh.run, bi.run)(Apply[Option].lift9(f)))
  def cramMap[A,B,C,D,E,F,G,H,I,J,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G],bh: Box[H],bi: Box[I],bj: Box[J])(f: (A,B,C,D,E,F,G,H,I,J) => Z): Box[Z] = OptionT(Apply[ListWriter].apply10(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run, bh.run, bi.run, bj.run)(Apply[Option].lift10(f)))
  def cramMap[A,B,C,D,E,F,G,H,I,J,K,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G],bh: Box[H],bi: Box[I],bj: Box[J],bk: Box[K])(f: (A,B,C,D,E,F,G,H,I,J,K) => Z): Box[Z] = OptionT(Apply[ListWriter].apply11(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run, bh.run, bi.run, bj.run, bk.run)(Apply[Option].lift11(f)))
  def cramMap[A,B,C,D,E,F,G,H,I,J,K,L,Z](ba: Box[A],bb: Box[B],bc: Box[C],bd: Box[D],be: Box[E],bf: Box[F],bg: Box[G],bh: Box[H],bi: Box[I],bj: Box[J],bk: Box[K],bl: Box[L])(f: (A,B,C,D,E,F,G,H,I,J,K,L) => Z): Box[Z] = OptionT(Apply[ListWriter].apply12(ba.run, bb.run, bc.run, bd.run, be.run, bf.run, bg.run, bh.run, bi.run, bj.run, bk.run, bl.run)(Apply[Option].lift12(f)))

  /* // Generate the cramMap definition string
  def makeCramMap(n: Int) = {
    require(n < 26, "Ran out of letters!")
    val types = 'A' until ('A'+n).toChar
    val argDecls = ('a' until ('a'+n).toChar zip types) map { case (c,t) => s"b$c: Box[$t]" }
    val applyParams = ('a' until ('a'+n).toChar) map { case c => s"b$c.run" }
    val typeStrings = types.mkString(",")

    s"  def cramMap[$typeStrings,Z](${argDecls.mkString(",")})(f: ($typeStrings) => Z): Box[Z] = OptionT(Apply[ListWriter].apply$n(${applyParams.mkString(", ")})(Apply[Option].lift$n(f)))"
  }
  */ // 1 to 12 foreach (i => println(makeCramOps(i)))
}
