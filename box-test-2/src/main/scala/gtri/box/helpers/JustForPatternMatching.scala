package gtri.box.helpers

import gtri.box._

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/15/12
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
object JustForPatternMatching {

  object FullBox {
    def unapply[A](b: Box[A]): Option[(A, List[Issue])] =
      b.run.value.map(_ -> b.run.written)
  }

  object EmptyBox {
    def unapply[A](b: Box[A]): Option[List[Issue]] =
      b.run.value match {
        case Some(_) => None
        case None => Some(b.run.written) // warning is wrong: https://issues.scala-lang.org/browse/SI-6771
      }
  }
}
