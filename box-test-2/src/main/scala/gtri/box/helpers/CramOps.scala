package gtri.box.helpers

import gtri.box._
import scalaz.OptionT
import scalaz.std.list.listMonoid

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/15/12
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
object CramOps {

  implicit class CramOps1[A](self: Box[(A)]) {
    def cram[Z](zz: Box[Z]): Box[(A,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a) <- os; z <- oz) yield (a,z) ): ListWriter[Option[(A,Z)]]
    }
  }

  implicit class CramOps2[A,B](self: Box[(A,B)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b) <- os; z <- oz) yield (a,b,z) ): ListWriter[Option[(A,B,Z)]]
    }
  }

  implicit class CramOps3[A,B,C](self: Box[(A,B,C)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c) <- os; z <- oz) yield (a,b,c,z) ): ListWriter[Option[(A,B,C,Z)]]
    }
  }

  implicit class CramOps4[A,B,C,D](self: Box[(A,B,C,D)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d) <- os; z <- oz) yield (a,b,c,d,z) ): ListWriter[Option[(A,B,C,D,Z)]]
    }
  }

  implicit class CramOps5[A,B,C,D,E](self: Box[(A,B,C,D,E)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e) <- os; z <- oz) yield (a,b,c,d,e,z) ): ListWriter[Option[(A,B,C,D,E,Z)]]
    }
  }

  implicit class CramOps6[A,B,C,D,E,F](self: Box[(A,B,C,D,E,F)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f) <- os; z <- oz) yield (a,b,c,d,e,f,z) ): ListWriter[Option[(A,B,C,D,E,F,Z)]]
    }
  }

  implicit class CramOps7[A,B,C,D,E,F,G](self: Box[(A,B,C,D,E,F,G)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g) <- os; z <- oz) yield (a,b,c,d,e,f,g,z) ): ListWriter[Option[(A,B,C,D,E,F,G,Z)]]
    }
  }

  implicit class CramOps8[A,B,C,D,E,F,G,H](self: Box[(A,B,C,D,E,F,G,H)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,H,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g,h) <- os; z <- oz) yield (a,b,c,d,e,f,g,h,z) ): ListWriter[Option[(A,B,C,D,E,F,G,H,Z)]]
    }
  }

  implicit class CramOps9[A,B,C,D,E,F,G,H,I](self: Box[(A,B,C,D,E,F,G,H,I)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,H,I,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g,h,i) <- os; z <- oz) yield (a,b,c,d,e,f,g,h,i,z) ): ListWriter[Option[(A,B,C,D,E,F,G,H,I,Z)]]
    }
  }

  implicit class CramOps10[A,B,C,D,E,F,G,H,I,J](self: Box[(A,B,C,D,E,F,G,H,I,J)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,H,I,J,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g,h,i,j) <- os; z <- oz) yield (a,b,c,d,e,f,g,h,i,j,z) ): ListWriter[Option[(A,B,C,D,E,F,G,H,I,J,Z)]]
    }
  }

  implicit class CramOps11[A,B,C,D,E,F,G,H,I,J,K](self: Box[(A,B,C,D,E,F,G,H,I,J,K)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,H,I,J,K,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g,h,i,j,k) <- os; z <- oz) yield (a,b,c,d,e,f,g,h,i,j,k,z) ): ListWriter[Option[(A,B,C,D,E,F,G,H,I,J,K,Z)]]
    }
  }

  implicit class CramOps12[A,B,C,D,E,F,G,H,I,J,K,L](self: Box[(A,B,C,D,E,F,G,H,I,J,K,L)]) {
    def cram[Z](zz: Box[Z]): Box[(A,B,C,D,E,F,G,H,I,J,K,L,Z)] = OptionT {
      ( for ( os <- self.run; oz <- zz.run ) yield
        for ( (a,b,c,d,e,f,g,h,i,j,k,l) <- os; z <- oz) yield (a,b,c,d,e,f,g,h,i,j,k,l,z) ): ListWriter[Option[(A,B,C,D,E,F,G,H,I,J,K,L,Z)]]
    }
  }

  /* // Generate the CramOps definition string
  def makeCramOps(n: Int) = {
    require(n < 26, "Ran out of letters!")
    val types = 'A' until ('A'+n).toChar
    val argDecls = ('a' until ('a'+n).toChar zip types) map { case (c,t) => s"b$c: Box[$t]" }
    val applyParams = ('a' until ('a'+n).toChar) map { case c => s"b$c.run" }
    val typeStrings = types.mkString(",")
    val lowerTypeStrings = typeStrings.toLowerCase
    s"""  implicit class CramOps$n[$typeStrings](self: Box[($typeStrings)]) {
      |    def cram[Z](zz: Box[Z]): Box[($typeStrings,Z)] = OptionT {
      |      ( for ( os <- self.run; oz <- zz.run ) yield
      |        for ( ($lowerTypeStrings) <- os; z <- oz) yield ($lowerTypeStrings,z) ): ListWriter[Option[($typeStrings,Z)]]
      |    }
      |  }
    """.stripMargin
  }
   */ // 1 to 12 foreach (i => println(makeCramOps(i)))
}
