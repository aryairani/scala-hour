package gtri.box

import scalaz._
import scalaz.syntax.monad._ // brings in Monad operators like >>
import scalaz.OptionT.optionTMonadPlus // Monad instance for OptionT
import scalaz.WriterT.writerTMonad // Monad instance for WriterT (OptionT is only a Monad when its F type is a Monad)
import std.list.listMonoid // Monoid instance for List  (WriterT is only a Monad when its W type is a Monoid)

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/15/12
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */

object `package` {
  type Issue = String
  type ListWriter[+A] = Writer[List[Issue],A]
  type Box[+A] = OptionT[ListWriter,A]

  implicit class BoxOps[A](b: Box[A]) {
    def isEmpty: Boolean = b.isDefined.value
    def nonEmpty = isEmpty == false
    def toOption: Option[A] = b.run.value
    def get: A = toOption.get

    def log: List[Issue] = b.run.written

    def copy(newLog : List[Issue]): Box[A]  = Box(b.run.mapWritten(_ => newLog))
    def append(item : Issue)                = Box(b.run :++> List(item))
    def append(moreItems : List[Issue])     = Box(b.run :++> moreItems)
    def prepend(item : Issue)               = Box(List(item) <++: b.run)
    def prepend(moreItems: List[Issue])     = Box(moreItems <++: b.run)
  }

  def examples {

    val b1: Box[Int]  = Box(1, List("issue1"))
    val b2            = Box(2, "issue2")

    import helpers.JustForPatternMatching._
    b2 match { // open a box to see if there is anything there
      case FullBox(item, log) =>
        println(item)
        log foreach { println(_) }
      case EmptyBox(log) =>
        log foreach { println(_) }
    }

    val b3 = b1 >> b2 // replace contents of b1 with contents of b2 (since they are both full) and append their logs
    val b4 = Box.empty[Int]("issue4")
    val b5 : Box[Int] = b1 >> b4 // doesn't replace b1 since b4 is empty, but logs are still appended
    val b6 : Box[String] = Box.empty("issue6") >> Box("asdf")

    import helpers.CramOps._
    val b7: Box[(Int,String)] = b1 cram b6 // cram the contents of two boxes together (box is only full if b1 and b6 are full also appends logs)
    val b8: Box[(Int,String,Int)] = b1 cram b6 cram b2 // cram more stuff into our boxes
    val b9: Box[String] = b8 flatMap { // apply a function to the crammed box b8
      case ((a : Int,b : String,c : Int)) => { // function is only called if b8 is full
        Box("asdf") // Box up a result - logs will be appended
      }
    } // b9 now has "asdf" and a concat of logs from b1,b6,b2 and itself

    import helpers.CramMap.cramMap
    // maybe with `cramMap` you won't need `cram`?
    case class XsdSchema(index: Int, name: String, length: Int)
    val b8m: Box[XsdSchema] = cramMap(b1,b6,b2)(XsdSchema.apply _)

  }
}
