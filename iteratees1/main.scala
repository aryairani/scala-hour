trait Input[+E] // E is the input type of the source
case class Element[E](e: E) extends Input[E]
case object Empty extends Input[Nothing]
case object EOF extends Input[Nothing]
 
/*
* IterV[E,A] represents a computation that can be in one of two states.
 * It can be Done, in which case it will hold a result (the accumulated value) of type A.
 * Or it can be waiting for more input of type E, in which case it will hold a
 * continuation that accepts the next input.
*/
trait IterV[E,A] {
  def run: A
}
case class Done[E,A](a: A, e: Input[E]) extends IterV[E,A] {
  def run : A = {
    a
  }
}
case class Cont[E,A](k: Input[E] => IterV[E,A]) extends IterV[E,A] {
  def run : A = {
    k(EOF).run
  }
}// continuation that accepts the next input
 
object Main {
  def enumerate[E,A]: (List[E], IterV[E,A]) => IterV[E,A] = {
    case (Nil, i) => i // if list is empty return iteratee
    case (_, i@Done(_, _)) => i // (list not empty) iteratee is done THEN return iteratee
    case (x :: xs, Cont(k)) => enumerate(xs, k(Element(x))) // (list not empty) get head(x) and tail(xs) and iteratee is in the continue state
  }
 
  def counter[A]: IterV[A,Int] = {
    def step(n: Int): Input[A] => IterV[A,Int] = {
      case Element(x) => Cont(step(n + 1))
      case Empty => Cont(step(n))
      case EOF => Done(n, EOF)
    }
    Cont(step(0))
  }
 
  def drop[E](n: Int): IterV[E,Unit] = {
    def step: Input[E] => IterV[E,Unit] = {
      case Element(x) => drop(n - 1)
      case Empty => Cont(step)
      case EOF => Done((), EOF)
    }
    if (n == 0) Done((), Empty) else Cont(step)
  }
 
  def head[E]: IterV[E, Option[E]] = {
    def step: Input[E] => IterV[E, Option[E]] = {
      case Element(x) => Done(Some(x), Empty)
      case Empty => Cont(step)
      case EOF => Done(None, EOF)
    }
    Cont(step)
  }
 
  def main(args : Array[String]) {
    val list = List(1,2,3)
    val result = enumerate(list, counter[Int]).run
    println("result=" + result)
    val result2 = enumerate(list, head[Int]).run
    println("result2=" + result2)
    val iterv3a = enumerate(list, counter[Int])
    val iterv3b = enumerate(List(9,10,11), iterv3a)
    println("result3=" + iterv3b.run)
  }
}