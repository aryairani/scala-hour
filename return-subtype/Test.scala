class A { def f() = this }
trait Mixin[T] { def f() : T }

object test {
  def foo[C,T <: Mixin[C]](t : T) = t.f()
  def bar() = foo[A, A with Mixin[A]](new A with Mixin[A])
}

object Attempt1 {

  trait Enumerator[O] {
    def isDone: Boolean
    def step() : Enumerator[O]
  }

  class IntShitter(val i : Int = 0) extends Enumerator[Int] {

    def isDone = i == 10

    // Notice how IntShitter overrides return value to return it's own type
    def step() : IntShitter = {
      new IntShitter(i + 1)
    }
  }

  object Runner {
    def run[O](e: Enumerator[O]) : Enumerator[O] = {
      var current = e
      while(!current.isDone) {
        current = current.step()
      }
      current
    }
  }

//  object Test extends App {
//    val shitter = new IntShitter()
////    Enumerator[Int] doesn't conform to IntShitter
//    val lastShitter : IntShitter = Runner.run(shitter)
//    println(lastShitter.i)
//  }

//  object Runner2 {
//    def run[O,E <: Enumerator[O]](e: E): E = {
//      var current : E = e
//      while(!current.isDone) {
//        current = current.step() // ERROR HERE -> Enumerator[O] doesn't conform to expected type E
//      }
//      current
//    }
//  }

  object Runner3 {
    type Structural[T] = { def isDone: Boolean; def step(): T }

    def run[E <: Structural[E]](e: E): E = {
      var current : E = e
      while (!current.isDone) {
        current = current.step()
      }
      current
    }
  }

  object Test1 extends App {
    val shitter = new IntShitter()
    val lastShitter : IntShitter = Runner3.run(shitter)
    println(lastShitter.i)
  }
}

object Attempt2 {

  trait Enumerator[O,E<:Enumerator[O,E]] {
    def isDone: Boolean
    def step() : E
  }

  class IntShitter(val i : Int = 0) extends Enumerator[Int,IntShitter] {
    def isDone = i == 10
    def step() : IntShitter = new IntShitter(i + 1)
  }

  object Runner4 {
    def run[O,E<:Enumerator[O,E]](e: E): E = {
      var current = e
      while (!current.isDone) {
        current = current.step()
      }
      current
    }
  }

  /*
  object Test2 extends App {
    val shitter = new IntShitter
    //  error: inferred type args [Nothing,Intshitter] do not conform to method run's type parameter bounds [O, E <: Enumerator[O,E]]
    val lastShitter = Runner4.run(shitter)
    println(lastShitter.i)
  }
  */
}

object Attempt3 {

  trait Enumerator[O] {
    type Newmerator <: Enumerator[O]
    def isDone : Boolean
    def step() : Newmerator
  }

  class IntShitter(i: Int) extends Enumerator[Int] {
    type Newmerator = IntShitter
    def isDone = (i == 10)
    def step() = new IntShitter(i + 1)
  }

//  object Runner {
//    def run[O, E <: Enumerator[O]](e: E) : E#Newmerator = {
//      if (e.isDone) e // E doesn't conform to type E#Newmerator
//      else {
//        var next = e.step()
//        while (!next.isDone)
//          next = next.step() // Enumerator.this.type#Newmerator != e.type#Newmerator
//        next
//      }
//    }
//  }
}

object Attempt4 {

  trait Enumerator[O] {
    type Newmerator <: this.type
    def isDone : Boolean
    def step() : Newmerator
  }

  /*
  class IntShitter(i: Int) extends Enumerator[Int] {
    type Newmerator = IntShitter // incompatible with this.type bound
    def isDone = (i == 10)
    def step() = new IntShitter(i + 1)
  }
  */

  //object Runner {
  //  def run[O, E <: Enumerator[O]](e: E) : E = {
  //    if (e.isDone) e
  //    else {
  //      var next: E = e.step()
  //      while (!next.isDone)
  //        next = next.step() // Enumerator.this.type#Newmerator != e.type#Newmerator
  //      next
  //    }
  //  }
  //}
}

object Attempt5 {

  trait Enumerator[E] {
    def isDone(e: E): Boolean
    def step(e: E): E
  }

  class EnumeratorOps[E](e: E)(implicit n:Enumerator[E]) {
    def isDone = n.isDone(e)
    def step() = n.step(e)
  }
  implicit def toEnumeratorOps[E:Enumerator](e: E) = new EnumeratorOps(e)

  case class IntShitter(i: Int = 0)

  implicit val ise = new Enumerator[IntShitter] {
    def isDone(e: IntShitter) = (e.i == 10)
    def step(e: IntShitter) = new IntShitter(e.i+1)
  }

  object Runner {
    def run[E:Enumerator](e: E) : E = {
      var current = e
      while (!current.isDone) {
        current = current.step()
      }
      current
    }
  }

  object Test extends App {
    val shitter = new IntShitter
    val lastShitter = Runner.run(shitter)
    println(lastShitter.i)
  }
}

object Attempt6 {

  trait Enumerator[E] { // typeclass
    def isDone(e: E): Boolean
    def step(e: E): E
  }

  // type IntShitter
  case class IntShitter(i: Int = 0)

  // type IntShitter is member of type class Enumerator
  implicit val ise = new Enumerator[IntShitter] {
    def isDone(e: IntShitter) = (e.i == 10)
    def step(e: IntShitter) = IntShitter(e.i+1)
  }

  def run[E](e: E)(implicit n: Enumerator[E]): E = {
    var current = e
    while (!n.isDone(current))
      current = n.step(current)
    current
  }

  def test1 {
    val shitter = new IntShitter
    val lastShitter = run(shitter)
    println(lastShitter.i)
  }

  implicit val sse = new Enumerator[String] {
    def isDone(s: String) = s.isEmpty

    def step(s: String) = {
      println(s)
      s.tail
    }
  }

  def test2 {
    println(run("Hello"))
  }
}


object Attempt7 {
  trait Enumerator[E] {
    def isDone(e: E): Boolean
    def step(e: E): E
  }

  implicit class EnumeratorOps[E](e: E)(implicit n:Enumerator[E]) {
    def isDone = n.isDone(e)
    def step() = n.step(e)
  }

  case class IntEnumerator(i: Int = 0)

  implicit val ise = new Enumerator[IntEnumerator] {
    def isDone(e: IntEnumerator) = (e.i == 10)
    def step(e: IntEnumerator) = new IntEnumerator(e.i+1)
  }

  def run[E:Enumerator](e: E) : E = {
    var current = e
    while (!current.isDone) {
      current = current.step()
    }
    current
  }

  object Test extends App {
    println(run(new IntEnumerator))
  }
}

object Attempt8 {

  // note, written as methods, not stand-alone functions
  trait Enumerator[E] {
    def isDone: Boolean  // as opposed to isDone(e: E): Boolean
    def step(): E
  }

  case class IntEnumerator(i: Int = 0)

  implicit class IntEnumeratorInstance(e: IntEnumerator) extends Enumerator[IntEnumerator] {
    def isDone = (e.i == 10)
    def step() = new IntEnumerator(e.i+1)
  }

  def run[E <% Enumerator[E]](e: E) : E = {
    var current = e
    while (!current.isDone) {
      current = current.step()
    }
    current
  }

  object Test extends App {
    println(run(new IntEnumerator))
  }
}