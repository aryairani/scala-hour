IntelliJ IDEA 13.1 now natively supports sbt projects, so just "open" one of these folders in IDEA to import it.

// Installing SBT yourself: 
// http://www.scala-sbt.org/release/docs/Getting-Started/Setup.html

Some extra exercises at https://github.com/refried/scala-fp-exercises
