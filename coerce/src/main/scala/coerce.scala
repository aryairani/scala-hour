import scala.util.Try

package object coerce {

  trait Coercion[A,B]       { def run: A => Option[B] }
  trait CoercionUnsafe[A,B] { def run: A => B }

  object Coercion {
    /** construct a coercion from a function with its own error handling */
    def make[A,B](f: A => Option[B]): Coercion[A,B] = new Coercion[A,B] { val run = f }

    /** construct a coercion with generic error handling */
    def makeSafe[A,B](f: A => B): Coercion[A,B] = new Coercion[A,B] { val run = (a:A) => Try(f(a)).toOption }

    /** construct a coercion with no error handling */
    def makeUnsafe[A,B](f: A => B): CoercionUnsafe[A,B] = new CoercionUnsafe[A,B] { val run = f }
  }
  
  object CoercionUnsafe {
    def make[A,B](f: A => B): CoercionUnsafe[A,B] = Coercion.makeUnsafe(f)

    /** safe conversions can be used in unsafe contexts */
    implicit def unsafeFromSafe[A,B](implicit safe: Coercion[A,B]): CoercionUnsafe[A,B] =
      new CoercionUnsafe[A,B] { val run = (a:A) => safe.run(a).get }
  }

  /** syntax */
  object syntax {

    /** Function Syntax */
    def coerce[A,B](a: A)(implicit e: Coercion[A,B]): Option[B] = e.run(a)
    def coerceUnsafe[A,B](a: A)(implicit e: CoercionUnsafe[A,B]): B = e.run(a)


    /** Object Syntax */
    implicit class CoercionOps[A](val a: A) extends AnyVal {
      def coerce[B](implicit e: Coercion[A,B]): Option[B] = e.run(a)
      def coerceUnsafe[B](implicit e: CoercionUnsafe[A,B]): B = e.run(a)
    }
  }


  // standard coercions defined here; can be imported individually, or you can define your own.
  import Coercion.makeSafe
  implicit val int2string = makeSafe[Int,String](_.toString)
  implicit val string2int = makeSafe[String,Int](Integer.parseInt(_))
}