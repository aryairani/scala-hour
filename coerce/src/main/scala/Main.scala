object Main extends App {
  import coerce.syntax._

  val s1: Option[String] = 3.coerce // Some("3")
  val s1u: String = 3.coerceUnsafe // "3"

  val i1: Option[Int] = "3".coerce // Some(3)
  val i1u: Int = "3".coerceUnsafe // 3

  val i2: Option[Int] = "hello".coerce // None
  val i2u: Int = "hello".coerceUnsafe // throws java.util.NoSuchElementException

//  val d1: Option[Double] = "3".coerce[Double]
//  error: could not find implicit value for parameter e: coerce.package.Coercion[String,Double]

}

