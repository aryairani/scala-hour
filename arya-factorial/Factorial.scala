object Factorial extends App {

  implicit class LongFactorial(x: Long) {
    require(x >= 0)

    def ! : Long = {
      if (x < 2) 1L
      else x * ((x-1)!) // creates new LongFactorial instances
    }
  }


  class LongFactorialv2(x: Long) {
    require(x >= 0)

    def ! : Long = {
      if (x < 2) 1L
      else x * ((x-1)!) // creates new LongFactorial instances
    }
  }
  implicit def LongFactorialv2(x: Long) = new LongFactorialv2(x)

  implicit def LongFactorialv3(x: Long) = new Object {
    def ! : Long = x
  }

  implicit def LongFactorialv4(x: Long) = new { def ! : Long = if (x < 2) 1L else x * ((x-1)!) }

  implicit def LongFactorialV5(x: Long) = new { def ! = (1 to x foldLeft (1:BigInt)) { _ * _ } }

}


object FactorialWithTypeclass extends App {
  import math.Ordering.Implicits._
  import math.Integral.Implicits._

  implicit class IntegralFactorialPimp[A:Integral](x:A) {
    require(x >= 0)

    /**
     * Converts integers (e.g. nearby integer constants 0, 1, 2) into the generic integral type A, to match X
     * @param x
     * @return
     */
    implicit def fromInt(x: Int): A = implicitly[Integral[A]].fromInt(x)

    def ! : A = {
      if (x < 2) 1
      else x * ((x - 1)!)
    }
  }

  println(11!)
}

object FactorialAnonymous extends App {

  implicit def xxxx(x: Int) = new {
    def ! = {
      require(x >= 0)
      (1 to x).foldLeft(1:BigInt)(_ * _)
    }
  }

  println(11!)
}