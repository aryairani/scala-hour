package pimpexample1

object Square {
  implicit class IntHelper(x:Int) {
    def square = x*x
  }
}

object SquareUsage extends App {
  import Square._
  val x = 3
  println(s"The square of $x is ${x.square}")
}