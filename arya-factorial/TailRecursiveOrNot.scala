object RecursiveFactorial extends App {
  def fac(x: BigInt, accumulator: BigInt = 1): BigInt = {
    if (x < 2) accumulator
    else fac(x-1, accumulator*x)
  }
  println(fac(6))
}

object TailRecursiveFactorial extends App {
  def fac(x: BigInt, accumulator: BigInt = 1): BigInt = {
    if (x < 2) accumulator
    else fac(x-1, accumulator*x)
  }
  println(fac(6))
}