package boxtest3

import org.scalatest.FunSpec

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/30/12
 * Time: 12:52 AM
 * To change this template use File | Settings | File Templates.
 */
class BoxMSpec extends FunSpec {

  import scalaz.syntax.monad._

  describe("A BoxM[M[+_],A]") {

    it("should be able to store an item (Go)") {
      val b1 : BoxM[Int] = BoxM(1)
      assert(b1 == Go(1))
      assert(b1.isGo)
      assert(b1.get == 1)
      val b2 : BoxM[Int] = Go(1)
      assert(b2 == Go(1))
    }

    it("should be able to be empty (NoGo)") {
      val b1 : BoxM[Int] = BoxM.empty
      assert(b1 == NoGo)
      assert(b1.isNoGo)
      val b2 : BoxM[Int] = NoGo
      assert(b2 == NoGo)
    }

    it("should be able to be empty but provide a method to recover from bad input (Recover)") {
      lazy val v : Int = { println("here"); 2 }
      val b1 : BoxM[Int] = BoxM.recover(v)
      assert(b1.isRecover)
      assert(b1.recoverable == v)
      val b2 : BoxM[Int] = BoxM.recover(v)
      assert(b2.isRecover)
    }

    it("should be able to recover from bad input") {
      lazy val v : Int = { println("here"); 2 }
      val b1 : BoxM[Int] = BoxM.recover(v)
      val result = b1.recover
      assert(result == Some(2))
    }

    it("should be able to convert to an Option") {
      val b1 : BoxM[Int] = BoxM(1)
      val opt1 : Option[Int] = b1.toOption
      assert(opt1 == Some(1))
      val b2 : BoxM[Int] = BoxM.empty
      val opt2 : Option[Int] = b2.toOption
      assert(opt2 == None)
      lazy val v : Int = { println("here"); 2 }
      val b3 : BoxM[Int] = BoxM.recover(v)
      val opt3 : Option[Int] = b3.toOption
      assert(opt3 == None)
    }

    it("should be able to extract its value using a for-comprehension") {
      val b1 : BoxM[Int] = BoxM(1)
      val b2 : BoxM[Int] = BoxM(2)
      val b3 : BoxM[Int] = BoxM(3)
      val bsum = for(a <- b1; b <- b2; c <- b3) yield a+b+c
      assert(bsum.isGo)
      assert(bsum.get == 6)
    }

    it("should return a NoGo in a for-comprehension if any BoxM is a NoGo") {
      val b1 : BoxM[Int] = BoxM(1)
      val b2 : BoxM[Int] = BoxM.empty
      val b3 : BoxM[Int] = BoxM(3)
      val bsum = for(a <- b1; b <- b2; c <- b3) yield a+b+c
      assert(bsum.isNoGo)
    }

    it("should return a Recover in a for-comprehension if any BoxM is a Recover") {
      lazy val v : Int = { println("here"); (2) }
      val b1 : BoxM[Int] = BoxM(1)
      val b2 : BoxM[Int] = BoxM.recover(v)
      val b3 : BoxM[Int] = BoxM(3)
      val bsum = for(a <- b1; b <- b2; c <- b3) yield a+b+c
      assert(bsum.isRecover)
    }

    it("should be able to return the result of a for-comprehension that can be recovered") {
      lazy val v2 : Int = { println("here"); (2) }
      lazy val v3 : Int = { println("here"); (3) }
      val b1 : BoxM[Int] = BoxM(1)
      val b2 : BoxM[Int] = BoxM.recover(v2)
      val b3 : BoxM[Int] = BoxM.recover(v3)
      val bsum = for(a <- b1; b <- b2; c <- b3) yield a+b+c
      assert(bsum.isRecover)
      val result = bsum.recover
      assert(result == Some(6))
    }

    it("should return None if any BoxM of a for-comprehension that can be recovered is NoGo") {
      lazy val v2 : Int = { println("here"); (2) }
      lazy val v3 : Int = { println("here"); (3) }
      val b1 : BoxM[Int] = BoxM(1)
      val b2 : BoxM[Int] = BoxM.recover(v2)
      val b3 : BoxM[Int] = BoxM.recover(v3)
      val b4 : BoxM[Int] = BoxM.empty
      val bsum = for(a <- b1; b <- b2; c <- b3; d <- b4) yield a+b+c+d
      assert(bsum.isRecover)
      val result = bsum.recover
      assert(result == None)
    }
  }

}