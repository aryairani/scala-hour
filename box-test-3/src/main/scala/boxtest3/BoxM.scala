package boxtest3

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/30/12
 * Time: 12:53 AM
 * To change this template use File | Settings | File Templates.
 */

sealed trait BoxM[+A]

/** Go */
final case class Go[+A](a: A) extends BoxM[A]

/** NoGo */
case object NoGo extends BoxM[Nothing]

/** Recover */
final class Recover[+A](lazyBox: => BoxM[A]) extends BoxM[A] {
  lazy val recoverable = lazyBox
}
object Recover {
//  def apply[A](__recoverable: => A) = new Recover(Go(__recoverable))
  def apply[A](lazyBox: => BoxM[A]) = new Recover(lazyBox)
  def unapply[A](box : BoxM[A]) : Option[() => BoxM[A]] = box match {
    case recover: Recover[A] => Some { () => recover.recoverable }
    case _ => None
  }
}

/** Constructors */
object BoxM {
  def apply[A](a: A) = Go(a)
  def empty = NoGo
  def recover[A](recoverable: => A) = Recover(Go(recoverable))
  def fromOption[A](opt : Option[A]) : BoxM[A] = opt match {
    case Some(a) => Go(a)
    case None => NoGo
  }
}