package boxtest3

import scalaz.Monad
import annotation.tailrec

//import Scalaz._

/**
 * Created with IntelliJ IDEA.
 * User: arya
 * Date: 12/30/12
 * Time: 12:55 AM
 * To change this template use File | Settings | File Templates.
 */
object `package` {
//  type Issue = String
//  type LogWriter[+A] = Writer[List[Issue],A]
//
//  object LogWriter {
//    def apply[A](a : A) : LogWriter[A] = apply(Nil, a)
//    def apply[A](issue : Issue, a : A) : LogWriter[A] = apply(List(issue), a)
//    def apply[A](issues : List[Issue], a : A) : LogWriter[A] = Writer(issues, a)
//  }
//
//  type Box[+A] = LogWriter[BoxM[A]]
//
//  object Box {
//    def empty[A] : Box[A] = empty[A](Nil)
//    def empty[A](issue : Issue) : Box[A] = empty[A](List(issue))
//    def empty[A](log : List[Issue]) : Box[A] = LogWriter(log, BoxM.empty)
//
//    def apply[A](a : A) : Box[A] = apply[A](Nil,a)
//    def apply[A](issue : Issue,a : A) : Box[A] = apply[A](List(issue),a)
//    def apply[A](log : List[Issue],a : A) : Box[A] = LogWriter(log,BoxM[A](a))
//
//    def recover[A](recoverable : => Box[A]) : Box[A] = recover[A](Nil,recoverable)
//    def recover[A](issue : Issue,recoverable : => Box[A]) : Box[A] = recover[A](List(issue),recoverable)
//    def recover[A](log : List[Issue],recoverable : => Box[A]) : Box[A] = {
//      lazy val r = {
//        val opt = recoverable.value.toOption
//        val log = recoverable.written
//        LogWriter(log,opt)
//      }
//      LogWriter(log, BoxM.recover[A](r))
//    }
//  }
//
//  implicit class boxAnything[A](self : A) {
//    def box : Box[A] = Box(self)
//    def box(issue : Issue) : Box[A] = box(List(issue))
//    def box(log : List[Issue]) : Box[A] = Box(log, self)
//  }

  implicit final class BoxMSyntax[A](box: BoxM[A]) {
    def isGo      = box match { case _ : Go[A] => true;      case _ => false }
    def isNoGo    = box match { case NoGo => true;           case _ => false }
    def isRecover = box match { case _ : Recover[A] => true; case _ => false }


    def get: A =
      box match {
        case Go(a) => a
        case _ => throw new NoSuchElementException
      }

    @tailrec def recoverable: A =
      box match {
        case Go(a) => a
        case Recover(thunk) => thunk().recoverable
        case _ => throw new IllegalStateException
      }

    @tailrec def recover: Option[A] = box match {
      case Go(a) => Some(a)
      case NoGo => None
      case Recover(thunk) => thunk().recover
    }

    def toOption: Option[A] = box match {
      case Go(a) => Some(a)
      case _ => None
    }

  }



  implicit object BoxMMonad extends Monad[BoxM] {
    def point[A](a: => A): BoxM[A] = Go(a)

    def bind[A, B](fa: BoxM[A])(f: (A) => BoxM[B]): BoxM[B] = fa match {
      case Go(a) => f(a)
      case NoGo => NoGo
      case Recover(thunk) => new Recover[B](
        thunk() match {
          case Go(a) => f(a)
          case NoGo => NoGo
          case Recover(inner) => bind(inner())(f)
        }
      )
    }
  }
}
