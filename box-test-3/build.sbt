scalaVersion := "2.10.0-RC5"

libraryDependencies += "org.scalaz" % "scalaz-core_2.10.0-RC5" % "7.0.0-M6"

libraryDependencies += "org.scalatest" % "scalatest_2.10.0-RC5" % "2.0.M5-B1"
